#![no_std]
#![no_main]

use esp_backtrace as _;
use esp_hal::{clock::ClockControl, i2c::I2C, peripherals::Peripherals, prelude::*, Delay, IO};
use esp_println::println;

use adxl345_eh_driver;
use bme280_rs::{Bme280, Configuration, Oversampling, SensorMode};

#[entry]
fn main() -> ! {
    let peripherals = Peripherals::take();
    let system = peripherals.SYSTEM.split();
    let clocks = ClockControl::max(system.clock_control).freeze();
    let mut delay = Delay::new(&clocks);
    let io = IO::new(peripherals.GPIO, peripherals.IO_MUX);

    let sda = io.pins.gpio5;
    let scl = io.pins.gpio6;
    let i2c = I2C::new(peripherals.I2C0, sda, scl, 100u32.kHz(), &clocks);

    let mut sensor_power_en_pin = io.pins.gpio7.into_push_pull_output();
    sensor_power_en_pin.set_high().unwrap();
    delay.delay_ms(100u32);

    let i2c_ref_cell = core::cell::RefCell::new(i2c);

    let mut accel = adxl345_eh_driver::Driver::new(
        embedded_hal_bus::i2c::RefCellDevice::new(&i2c_ref_cell),
        Some(adxl345_eh_driver::address::SECONDARY),
    )
    .unwrap();
    let mut bme280 = Bme280::new_with_address(
        embedded_hal_bus::i2c::RefCellDevice::new(&i2c_ref_cell),
        0x77,
        delay,
    );
    bme280.init().unwrap();
    bme280
        .set_sampling_configuration(
            Configuration::default()
                .with_temperature_oversampling(Oversampling::Oversample1)
                .with_pressure_oversampling(Oversampling::Oversample1)
                .with_humidity_oversampling(Oversampling::Oversample1)
                .with_sensor_mode(SensorMode::Normal),
        )
        .unwrap();

    loop {
        println!("Acceleration: {:?}", accel.get_accel());
        delay.delay_ms(1000u32);

        if let Some(temperature) = bme280.read_temperature().unwrap() {
            println!("Temperature: {} C", temperature);
        } else {
            println!("Temperature reading was disabled");
        }
        delay.delay_ms(1000u32);
    }
}
