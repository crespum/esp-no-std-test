#![no_std]
#![no_main]
// #![feature(type_alias_impl_trait)]

use embassy_executor::Spawner;
use embassy_sync::blocking_mutex::raw::NoopRawMutex;
use embassy_sync::mutex::Mutex;
use embassy_time::{Delay, Duration, Timer};

use esp_backtrace as _;
use esp_hal::{
    clock::ClockControl, embassy, i2c::I2C, peripherals::Peripherals, prelude::*,
    timer::TimerGroup, IO,
};
use esp_println::println;

use bme280_rs::{AsyncBme280, Configuration, Oversampling, SensorMode};

#[main]
async fn main(_spawner: Spawner) {
    let peripherals = Peripherals::take();
    let system = peripherals.SYSTEM.split();
    let clocks = ClockControl::max(system.clock_control).freeze();

    let timg0 = TimerGroup::new(peripherals.TIMG0, &clocks);
    embassy::init(&clocks, timg0);

    let io = IO::new(peripherals.GPIO, peripherals.IO_MUX);

    let sda = io.pins.gpio5;
    let scl = io.pins.gpio6;
    let i2c = I2C::new(peripherals.I2C0, sda, scl, 100u32.kHz(), &clocks);
    let i2c_mutex: Mutex<NoopRawMutex, _> = Mutex::new(i2c);

    let mut sensor_power_en_pin = io.pins.gpio7.into_push_pull_output();
    sensor_power_en_pin.set_high().unwrap();
    Timer::after(Duration::from_millis(100)).await;

    let i2c_bus_1 = embassy_embedded_hal::shared_bus::asynch::i2c::I2cDevice::new(&i2c_mutex);
    let mut accel =
        adxl345_eh_driver::Driver::new(i2c_bus_1, Some(adxl345_eh_driver::address::SECONDARY))
            .await
            .unwrap();

    let i2c_bus_2 = embassy_embedded_hal::shared_bus::asynch::i2c::I2cDevice::new(&i2c_mutex);
    let mut bme280 = AsyncBme280::new_with_address(i2c_bus_2, 0x77, Delay);
    bme280.init().await.unwrap();
    bme280
        .set_sampling_configuration(
            Configuration::default()
                .with_temperature_oversampling(Oversampling::Oversample1)
                .with_pressure_oversampling(Oversampling::Oversample1)
                .with_humidity_oversampling(Oversampling::Oversample1)
                .with_sensor_mode(SensorMode::Normal),
        )
        .await
        .unwrap();

    loop {
        println!(
            "Acceleration: {:?}",
            accel.get_accel().await.unwrap_or_default()
        );
        Timer::after(Duration::from_millis(1000)).await;

        if let Some(temperature) = bme280.read_temperature().await.unwrap() {
            println!("Temperature: {} C", temperature);
        } else {
            println!("Temperature reading was disabled");
        }
        Timer::after(Duration::from_millis(1000)).await;
    }
}
