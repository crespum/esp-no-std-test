# Comparing no_std sync/async

With this project I want to explore the difference between sync (blocking) and async Rust as well as get some hands on experience.

## Hardware
A subset of the dark-sky-meter board has been used for test:

* Accelerometer (ADXL2345)
* Environment (BME280)

## Firmware
Both examples read the ADXL2345 and the BME280 in a continuous loop, every 1 seconds.

## Power consumption results
![](./async/static/async_pm.png)
![](./sync/static/sync_pm.png)